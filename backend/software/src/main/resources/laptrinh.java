public class ATMStrategy implements PayStrategy {

    private String name;
    private String Number;

    private String dateOfExpiry;

    public CreditCardStrategy(String name, String Num, String expireDate) {
        this.name = name;
        this.Number = Num;
        this.dateOfExpiry = expireDate;

    }

    @0verride

    public void pay(int amount) {
        System.out.println(amount + "paid with atm card");
    }
}

public class Item {

    private String upcCode;
    private int price;

    public Item(String upc, int cost) {
        this.upceCode = upc;

        this.price = cost;

    }

    public String getUpcCode() {
        return upcCode;
    }

    public int getPrice() {
        return price;
    }
}

public interface PaymentStrategy {

    void pay(int amount);
}

public class PaypalStrategy implements PaymentStrategy {

    private String emailld;
    private String password;

    public PaypalStrategy(String email, String pwd) {
        this.emailId = email;
        this.password = pwd;

    }

    @0verride

    public void pay(int amount) {
        System.out.println(amount + "paid using paypal.");
    }
}

public class ShoppingCart {
    private List<Item> items = new ArrayList<>();

    public void addItem(Item item) {
        this.items.add(item);
    }

    public void removeltem(Item item) {
        this. items. remove(item);
    }

    public int calculateTotal() {
        int sum = 0;
        for (Item item : items) {
            sum += item.getPrice();
        }

        return sum;

    }

    public void pay(PaymentStrategy paymentMethod) {
        int amount = calculateTotal();
        paymentMethod. pay (amount) ;
    }
}

public class StrategyPatternExample {

    public static void main(String[] args) {
        ShoppingCart cart = new ShoppingCart();

        Item item1 = new Item("Item1", 48);
        Item item2 = new Item("Item2", 58);

        cart.addItem(item1);
        cart.addItem(item2);
        cart.pay(new PaypalStrategy("vietanh", "PWD"));
        cart.pay(new CreditCardStrategy("vietanh", "123456", "102", "18/09"));
    }
}
