import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import ListNews from '../views/news/ListNews.vue'
import NewsDetail from "@/views/news/NewsDetail"
import Search from "@/views/news/Search";
import NotFound from "@/views/NotFound";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/listNews/:id',
    name: 'listNews',
    component: ListNews,
    props:true
  },
  {
    path: '/detail/:id',
    name: 'NewsDetail',
    component: NewsDetail,
    props:true
  }
  ,
  {
    path: '/search/:name',
    name: 'Search',
    component: Search,
    props:true
  },
  {
    path: '/:catchAll(.*)',
    name:'NotFound',
    component: NotFound
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
