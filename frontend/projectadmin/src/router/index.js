import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import NotFound from "@/views/NotFound";
import Statis from "@/views/admin/Statis";
import News from "@/views/admin/News";
import NewsKind from "@/views/admin/NewsKind";
import User from "@/views/admin/User";
import addUser from "@/views/admin/addUser";
import addNewsKind from "@/views/admin/addNewsKind";
import NewsKindEdit from "@/views/admin/NewsKindEdit";
import UserEdit from "@/views/admin/UserEdit";
import addNews from "@/views/admin/addNews";
import NewsEdit from "@/views/admin/NewsEdit";
import Login from "@/views/user/Login";
import Register from "@/views/user/Register";
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/statis',
    name: 'Statis',
    component: Statis
  },{
    path: '/news',
    name: 'News',
    component: News
  },
  {
    path: '/add-news',
    name: 'addNews',
    component: addNews
  },{
    path: '/news/:id',
    name: 'NewsEdit',
      component: NewsEdit
  },{
    path: '/news-kind',
    name: 'Newskind',
    component: NewsKind
  },
  {
    path: '/add-news-kind',
    name: 'addNewsKind',
    component: addNewsKind
  },{
    path: '/news-kind/:id',
    name: 'NewskindEdit',
    component: NewsKindEdit
  }
  ,{
    path: '/user',
    name: 'User',
    component: User
  },
  {
    path: '/add-user',
    name: 'addUser',
    component: addUser
  },{
    path: '/user/:id',
    name: 'UserEdit',
    component: UserEdit
  },

  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {
    path: '/:catchAll(.*)',
    name:'NotFound',
    component: NotFound
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})


export default router
