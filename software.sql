-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 15, 2022 lúc 02:37 PM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `software`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_address` varchar(255) DEFAULT NULL,
  `admin_desc` varchar(255) DEFAULT NULL,
  `admin_email` varchar(255) DEFAULT NULL,
  `admin_name` varchar(255) DEFAULT NULL,
  `create_at` datetime(6) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin_age` int(11) DEFAULT NULL,
  `admin_country` varchar(255) DEFAULT NULL,
  `admin_salary` int(11) DEFAULT NULL,
  `employee_group` int(11) DEFAULT NULL,
  `knowledge` varchar(255) DEFAULT NULL,
  `admin_role_id` int(11) DEFAULT NULL,
  `gender_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_address`, `admin_desc`, `admin_email`, `admin_name`, `create_at`, `password`, `admin_age`, `admin_country`, `admin_salary`, `employee_group`, `knowledge`, `admin_role_id`, `gender_id`) VALUES
(1, 'Ha Noi', 'Mo ta', 'admin@gmail.com', 'vanh', '2021-05-18 22:29:13.000000', '12345678', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'Ha Noi', 'VietAnh', 'lonlywolf189@gmail.com', 'vietanh123', '2021-06-17 19:30:11.000000', '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'Ha Noi', '123', 'student1@gmail.com', 'vanh', '2022-03-14 08:38:21.000000', '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin_news`
--

CREATE TABLE `admin_news` (
  `admin_news_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `news_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin_role`
--

CREATE TABLE `admin_role` (
  `admin_role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comment`
--

CREATE TABLE `comment` (
  `comment_id` int(11) NOT NULL,
  `comment_content` varchar(255) DEFAULT NULL,
  `lever` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `news_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `gender`
--

CREATE TABLE `gender` (
  `gender_id` int(11) NOT NULL,
  `gender_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `gender`
--

INSERT INTO `gender` (`gender_id`, `gender_name`) VALUES
(1, 'nam'),
(2, 'Nu');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `history`
--

CREATE TABLE `history` (
  `history_id` int(11) NOT NULL,
  `like_number` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `news_id` int(11) NOT NULL,
  `content` text DEFAULT NULL,
  `like_count` int(11) DEFAULT NULL,
  `news_date` datetime(6) DEFAULT NULL,
  `news_desc` text DEFAULT NULL,
  `news_source` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_img` varchar(255) DEFAULT NULL,
  `news_kind_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`news_id`, `content`, `like_count`, `news_date`, `news_desc`, `news_source`, `title`, `title_img`, `news_kind_id`) VALUES
(13, '<div class=\"journal-content-article\">\n<p><strong>Theo Nghị quyết về chính sách ưu đãi thuế thúc đẩy phát triển và ứng dụng CNTT tại Việt Nam vừa được Chính phủ ban hành, giảm 50% số thuế thu nhập cá nhân phải nộp đối với thu nhập từ tiền lương, tiền công của các cá nhân nhân lực công nghệ cao làm việc trong lĩnh vực CNTT.</strong></p>\n<p>Nghị quyết 41 của Chính phủ về chính sách ưu đãi thuế thúc đẩy việc phát triển và ứng dụng CNTT tại Việt Nam đã được Thủ tướng Chính phủ Nguyễn Xuân Phúc ký ban hành ngày 26/5/2016.</p>\n<p>Nghị quyết nêu rõ, để nâng cao sức cạnh tranh của doanh nghiệp CNTT, đẩy mạnh thu hút đầu tư phục vụ cho phát triển CNTT theo các mục tiêu đã đặt ra trong điều kiện hội nhập quốc tế ngày càng sâu rộng, cần thiết phải có thêm các chính sách hỗ trợ, trong đó có chính sách ưu đãi thuế nhằm thúc đẩy hơn nữa ứng dụng và phát triển CNTT tại Việt Nam.</p>\n<p><img src=\"http://vnreview.vn/image/15/30/82/1530822.jpg?t=1464360732166\" alt=\"\"></p>\n<p>Cụ thể, theo Nghị quyết này, với các giải pháp về chính sách ưu đãi thuế thuộc thẩm quyền của Quốc hội, bổ sung thu nhập của doanh nghiệp từ thực hiện các dự án: sản xuất sản phẩm nội dung thông tin số, dịch vụ phần mềm, sản xuất sản phẩm CNTT trọng điểm, dịch vụ khắc phục sự cố an toàn thông tin, bảo vệ an toàn thông tin được ưu đãi thuế thu nhập doanh nghiệp như mức đang áp dụng đối với dự án sản xuất sản phẩm phần mềm theo quy định của Luật thuế thu nhập doanh nghiệp.</p>\n<p>Bên cạnh đó, giảm 50% số thuế thu nhập cá nhân phải nộp đối với thu nhập từ tiền lương, tiền công của các cá nhân nhân lực công nghệ cao làm việc trong lĩnh vực CNTT, là đội ngũ những người có trình độ và kỹ năng đáp ứng được yêu cầu của hoạt động nghiên cứu, phát triển, ứng dụng công nghệ cao trong lĩnh vực CNTT; dịch vụ công nghệ cao trong lĩnh vực CNTT, quản lý hoạt động công nghệ cao trong lĩnh vực CNTT; vận hành các thiết bị, dây chuyền sản xuất sản phẩm công nghệ cao trong lĩnh vực CNTT; quản lý an toàn hệ thống thông tin.</p>\n<p>Đối với các giải pháp về chính sách ưu đãi thuế thuộc thẩm quyền của Chính phủ, Nghị quyết 41 quy định, bổ sung các hoạt động CNTT cần đặc biệt khuyến khích là: sản xuất và dịch vụ phần mềm; dịch vụ thiết kế, tư vấn CNTT; dịch vụ tích hợp hệ thống; dịch vụ quản lý, duy trì hệ thống CNTT (ứng dụng, mạng, thiết bị); dịch vụ thuê ngoài hệ thống CNTT; dịch vụ bảo mật hệ thống thông tin không sử dụng mật mã dân sự; dịch vụ xử lý, khai thác dữ liệu cơ sở dữ liệu; dịch vụ tìm kiếm thông tin trên mạng; dịch vụ trung tâm dữ liệu Data center; dịch vụ thuê ngoài BPO, KPO cho xuất khẩu; dịch vụ chứng thực chữ ký điện tử vào Danh mục hoạt động thuộc lĩnh vực nghiên cứu khoa học và phát triển công nghệ, Danh mục công nghệ cao được ưu tiên đầu tư phát triển và Danh mục sản phẩm công nghệ cao được khuyến khích phát triển.</p>\n<p>Trường hợp dự án đầu tư mới thuộc lĩnh vực CNTT cần đặc biệt khuyến khích đầu tư nêu trên có sử dụng thường xuyên trên 1.000 lao động (kể cả trường hợp dự án đã hết thời gian gian 15 năm hưởng thuế suất 10%), được kéo dài thêm thời gian áp dụng mức thuế suất 10% trong 15 năm.</p>\n<p>Chính phủ giao Bộ Tài chính hướng dẫn thực hiện ưu đãi thuế thu nhập doanh nghiệp đối với hoạt động CNTT thuộc Danh mục lĩnh vực nghiên cứu khoa học và phát triển công nghệ, Danh mục công nghệ cao được ưu tiên đầu tư phát triển và Danh mục sản phẩm công nghệ cao được khuyến khích phát triển theo quy định của Thủ tướng Chính phủ, hoàn thành trong tháng 10/2016.</p>\n<p>Bộ Tài chính cũng chịu trách nhiệm chủ trì, phối hợp với Bộ Tư pháp, Bộ TT&amp;TT và các Bộ, ngành liên quan trình Chính phủ báo cáo Quốc hội việc sửa đổi, bổ sung các Luật thuế thu nhập doanh nghiệp, Luật thuế thu nhập cá nhân để thực hiện các giải pháp trên vào thời điểm thích hợp; đồng thời đẩy mạnh công tác thông tin, tuyên truyền các giải pháp về thuế tại Nghị quyết 41 để tổ chức, cá nhân biết và theo dõi, giám sát việc thực hiện.</p>\n<p>Chính phủ phân công Bộ TT&amp;TT xây dựng và trình Thủ tướng Chính phủ ban hành Quyết định quy định thí điểm quản lý hoạt động xuất khẩu, nhập khẩu sản phẩm phần mềm, dịch vụ phần mềm và nội dung thông tin số, hoàn thành trong tháng 8/2016.</p>\n<p>Bộ TT&amp;TT cũng có trách nhiệm chủ trì, phối hợp với các Bộ, ngành liên quan tiếp&nbsp; tục nghiên cứu, xây dựng tiêu chí, điều kiện xác định cụ thể đối với một số hoạt động dịch vụ phần mềm quan trọng để được hưởng ưu đãi thuế thu nhập doanh nghiệp khi trình Chính phủ, trình Quốc hội sửa đổi, bổ sung Luật thuế thu nhập doanh nghiệp.</p>\n<p>Bộ KH&amp;CN có trách nhiệm trình Thủ tướng Chính phủ quyết định bổ sung các hoạt động CNTT cần đặc biệt khuyến khích được nêu tại Nghị quyết vào Danh mục hoạt động thuộc lĩnh&nbsp; vực nghiên cứu khoa học và phát triển công nghệ, Danh mục công nghệ cao được ưu tiên đầu tư phát triển và Danh mục sản phẩm công nghệ cao được khuyến khích phát triển. Thời gian hoàn thành là trong tháng 8/2016.</p>\n<p>Chính phủ còn giao Bộ KH&amp;CN phối hợp cùng Bộ Tài chính, Bộ TT&amp;TT và các Bộ, ngành liên quan xây dựng tiêu chí, điều kiện xác định cụ thể đối với nhân lực công nghệ cao để xem xét hưởng ưu đãi thuế thu nhập cá nhân khi trình Chính phủ, trình Quốc hội sửa đổi, bổ sung Luật Thuế thu nhập cá nhân, đảm bảo phù hợp với yêu cầu thực tiễn, đảm bảo chính sách ưu đãi đúng đối tượng.</p>\n<p align=\"right\"><em>Theo ICTnews</em></p>\n</div>', 0, '2021-05-18 20:58:40.000000', 'test', NULL, 'Giảm 50% thuế thu nhập cá nhân cho nhân lực công nghệ cao làm CNTT', '/api/admin/news/downloadFile/1528991.jpg', 1),
(15, '<div id=\"aui_3_2_0_1362\" class=\"journal-content-article\">\n<p><strong>Trong một thông báo phát đi ngày thứ Năm 26/5, Facebook cho biết động thái trên là một trong những nỗ lực mở rộng mạng lưới quảng cáo trực tuyến của mạng xã hội lớn nhất thế giới này.</strong></p>\n<p><img src=\"http://vnreview.vn/image/15/30/87/1530875.jpg?t=1464406280974\" alt=\"\"></p>\n<p><em>Facebook sẽ hiển thị các quảng cáo tới người dùng web không phải là thành viên của mạng xã hội này.</em></p>\n<p>Theo<em> Nhật báo Phố Wall,</em> Facebook sẽ sử dụng các tập tin cookie (lưu trữ thông tin duyệt web), nút “like”, và các plug-in khác nhúng vào trang web của bên thứ ba để theo dõi các thành viên và cả những người dùng không phải là thành viên của mạng xã hội này.</p>\n<p>Trong khi bị các nhà chức trách châu Âu chỉ trích là vi phạm quyền riêng tư cá nhân, Facebook lại cho rằng hành động trên của họ mang tới nhiều lợi ích và những quảng cáo liên quan cho người duyệt web không phải là thành viên mạng xã hội.</p>\n<p id=\"aui_3_2_0_1361\"><em id=\"aui_3_2_0_1359\">Theo Vietnamplus</em></p>\n</div>', 0, '2021-05-18 22:11:12.000000', 'test', NULL, 'Facebook theo dõi người không có tài khoản Facebook', '/api/admin/news/downloadFile/1528991.jpg', 1),
(16, '<div class=\"journal-content-article\">\n<p><strong>Đây là 7 tính năng dự kiến sẽ gây ấn tượng trên Android N, phiên bản Android mới nhất của Google sẽ được phát hành chính thức vào mùa thu năm nay.</strong></p>\n<p><strong><img src=\"http://vnreview.vn/image/15/28/99/1528991.jpg?t=1463973733186\" alt=\"\"></strong></p>\n<p>Theo <em>Cnet</em>, phiên bản thử nghiệm Developer Preview đầu tiên của Android N được Google giới thiệu hồi tháng 3 với nhiều cải tiến đáng giá. Sau 2 tháng thử nghiệm, các tính năng mới trên Android N đã được Google chia sẻ nhiều hơn tại hội nghị Google I/O 2016 vừa qua.</p>\n<p>Dù giao diện không thay đổi so với các phiên bản trước, nhưng Android N được Google bổ sung rất nhiều tính năng hữu ích giúp nâng cao trải nghiệm người dùng một cách tối đa như trợ lý ảo hoàn toàn mới, tăng cường bảo mật, bổ sung các biểu tượng cảm xúc (emoji) mới cùng rất nhiều tính năng khác. Sau đây là 7 tính năng mới được nhiều người mong đợi nhất trên Android N, phiên bản Android mới nhất của Google.</p>\n<p><strong>1. Trợ lý ảo Google Assistant</strong></p>\n<p><strong><img src=\"http://vnreview.vn/image/15/28/99/1528994.jpg?t=1463973733186\" alt=\"\"></strong></p>\n<p>Được tạo ra nhằm thay thế cho Google Now, <a href=\"http://vnreview.vn/tin-tuc-san-pham-moi/-/view_content/content/1836896/google-gioi-thieu-tro-li-ao-moi-google-assistant\" target=\"_blank\">Google Assistant</a> sẽ trả lời các câu hỏi cũng như thực hiện các yêu cầu của người dùng dựa trên cơ sở tìm kiếm Google và nền tảng Android. Mang đến người dùng cảm giác tương tác một cách tự nhiên, thân thiện hơn cũng chính là điểm khác biệt giữa Google Assistant và Google Now.</p>\n<p><strong>2. Instant Apps</strong></p>\n<p><iframe src=\"https://gfycat.com/ifr/DistortedOrdinaryBeauceron\" width=\"640\" height=\"1306.12\" frameborder=\"0\" scrolling=\"no\" allowfullscreen=\"allowfullscreen\"></iframe></p>\n<p>Instant Apps cho phép người dùng chạy trực tiếp một số ứng dụng liên kết ngay từ trình duyệt web mà không cần cài đặt vào máy. Tính năng này cực kỳ hữu ích cho những người thích mua sắm trên mạng, bởi bạn sẽ không phải chuyển từ trình duyệt web sang ứng dụng cho di động của cửa hàng đó nữa. Instant Apps dự kiến ra mắt vào cuối năm nay, hỗ trợ Android 4.1 trở lên.</p>\n<p><strong>3. Chia đôi màn hình</strong></p>\n<p><strong><img src=\"http://vnreview.vn/image/15/28/99/1528997.jpg?t=1463973733186\" alt=\"\"></strong></p>\n<p><em>Ảnh: Google</em></p>\n<p>Với sự hỗ trợ ngay từ phía Google, tính năng chia đôi màn hình để sử dụng 2 ứng dụng cùng lúc trên các thiết bị di động dự kiến sẽ trở thành một tiêu chuẩn mới trong tương lai (mặc dù đã được các hãng bên thứ 3 như Samsung, LG áp dụng từ trước). Ngoài Android, một số thiết bị iPad cao cấp của Apple cũng đã được trang bị tính năng này nhờ vào iOS 9.</p>\n<p>Ngoài tính năng chia đôi màn hình, Google cũng sẽ mang picture-in-picture lên Android N, cho phép thu nhỏ cửa sổ xem video hiện tại thành một pop-up nổi trên màn hình có thể di chuyển hoặc thay đổi kích thước, giúp bạn vừa xem video vừa có thể chạy 1 ứng dụng khác.</p>\n<p><strong>4. Trả lời tin nhắn từ thanh thông báo</strong></p>\n<p><img src=\"http://vnreview.vn/image/15/29/00/1529000.jpg?t=1463973733186\" alt=\"\"></p>\n<p>Được mang lên từ Android Wear, tính năng này cho phép bạn trả lời tin nhắn trực tiếp từ thanh thông báo mà không phải truy cập ứng dụng nhắn tin như trước.</p>\n<p><strong>5. Nhóm thông báo theo ứng dụng</strong></p>\n<p><img src=\"http://vnreview.vn/image/15/29/00/1529003.jpg?t=1463973733186\" alt=\"\"></p>\n<p>Các nhà phát triển có thể gom nhóm các thông báo được gửi đến thiết bị từ ứng dụng của họ thành 1 thông báo duy nhất, và người dùng có thể nhấn vào nó để xem toàn bộ các thông báo còn lại trong ứng dụng đó. Tính năng này rất phù hợp cho những người suốt ngày phải xóa một mớ thông báo hỗn độn bằng tay.</p>\n<p><strong>6. Tiết kiệm pin Doze on the Go</strong></p>\n<p>Xuất hiện từ Android 6.0 Marshmallow, Doze là chế độ tiết kiệm pin đặc biệt sẽ tắt các ứng dụng chạy nền đi khi điện thoại không được sử dụng trong một khoảng thời gian nhất định. Doze on the Go cũng tương tự, nhưng nó sẽ bị vô hiệu hóa nếu phát hiện chiếc điện thoại đang chuyển động.</p>\n<p>Ngoài Doze on the Go, Google còn đang phát triển Project Svelte giúp thiết bị quản lý việc sử dụng bộ nhớ một cách thông minh hơn và tiết kiệm pin hơn. Mục đích là để mang những bản cập nhật lớn đến các thiết bị có bộ nhớ trong hạn hẹp một cách dễ dàng hơn.</p>\n<p><strong>7. Night Mode</strong></p>\n<p><strong><img src=\"http://vnreview.vn/image/15/29/00/1529006.jpg?t=1463973733186\" alt=\"\"></strong></p>\n<p>Cũng giống như Apple Night Shift, Night Mode trên Android N sẽ giúp người dùng đỡ mỏi mắt hơn khi sử dụng máy vào ban đêm bằng cách điều chỉnh các ánh sáng xanh từ màn hình sang ánh sáng vàng giúp người dùng dễ ngủ hơn so với khi dùng 1 thiết bị trong buổi tối với ánh sáng xanh. Bạn cũng có thể điều chỉnh độ sáng cũng như độ “vàng” của màn hình.</p>\n<p align=\"right\"><strong>Phúc Thịnh</strong></p>\n</div>', 0, '2021-05-18 22:12:00.000000', 'test', NULL, '7 tính năng được kỳ vọng trên Android N', '/api/admin/news/downloadFile/1528991.jpg', 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news_kind`
--

CREATE TABLE `news_kind` (
  `news_kind_id` int(11) NOT NULL,
  `news_kind_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `news_kind`
--

INSERT INTO `news_kind` (`news_kind_id`, `news_kind_name`) VALUES
(1, 'Sức Khỏe'),
(2, 'Kinh tế vĩ mô'),
(3, 'Kinh doanh công nghệ');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `auth_provider` varchar(255) DEFAULT NULL,
  `enable` bit(1) DEFAULT NULL,
  `user_address` varchar(255) DEFAULT NULL,
  `user_birth` date DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `user_pass` varchar(255) DEFAULT NULL,
  `user_phone` int(11) DEFAULT NULL,
  `gender_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`user_id`, `auth_provider`, `enable`, `user_address`, `user_birth`, `user_email`, `user_name`, `user_pass`, `user_phone`, `gender_id`) VALUES
(1, '123456', b'1', 'vanh@gmail.com', '2021-05-05', 'vanh@gmail.com', 'Vanh', '123456', 123456, 1),
(2, '123456', b'1', 'admin@gmail.com', '2021-04-27', 'admin@gmail.com', 'Admin', '123456', 123, 2);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`),
  ADD UNIQUE KEY `UKb79jyjd00uj4gvvtu7favkq5g` (`admin_email`),
  ADD KEY `FKi0juobyt1j7prufw3k7g3x89q` (`admin_role_id`),
  ADD KEY `FK9neda4ldxu0vnvdndpmaey23n` (`gender_id`);

--
-- Chỉ mục cho bảng `admin_news`
--
ALTER TABLE `admin_news`
  ADD PRIMARY KEY (`admin_news_id`),
  ADD KEY `FKdq2lin3wa8wwwe9g0cf7mnwar` (`admin_id`),
  ADD KEY `FKf0br28rwtb4q05enyoswn72la` (`news_id`);

--
-- Chỉ mục cho bảng `admin_role`
--
ALTER TABLE `admin_role`
  ADD PRIMARY KEY (`admin_role_id`);

--
-- Chỉ mục cho bảng `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `FKnxm8x9npdhuwxv2x2wxsghm17` (`news_id`),
  ADD KEY `FK8kcum44fvpupyw6f5baccx25c` (`user_id`);

--
-- Chỉ mục cho bảng `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`gender_id`);

--
-- Chỉ mục cho bảng `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`history_id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`news_id`),
  ADD KEY `FKgur68ld5yqiejudnqppqgaro5` (`news_kind_id`);

--
-- Chỉ mục cho bảng `news_kind`
--
ALTER TABLE `news_kind`
  ADD PRIMARY KEY (`news_kind_id`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `FKcbf93j56y7t2tyhunb4neewva` (`gender_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT cho bảng `history`
--
ALTER TABLE `history`
  MODIFY `history_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT cho bảng `news_kind`
--
ALTER TABLE `news_kind`
  MODIFY `news_kind_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `FK9neda4ldxu0vnvdndpmaey23n` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`gender_id`),
  ADD CONSTRAINT `FKi0juobyt1j7prufw3k7g3x89q` FOREIGN KEY (`admin_role_id`) REFERENCES `admin_role` (`admin_role_id`);

--
-- Các ràng buộc cho bảng `admin_news`
--
ALTER TABLE `admin_news`
  ADD CONSTRAINT `FKdq2lin3wa8wwwe9g0cf7mnwar` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`),
  ADD CONSTRAINT `FKf0br28rwtb4q05enyoswn72la` FOREIGN KEY (`news_id`) REFERENCES `news` (`news_id`);

--
-- Các ràng buộc cho bảng `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK8kcum44fvpupyw6f5baccx25c` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `FKnxm8x9npdhuwxv2x2wxsghm17` FOREIGN KEY (`news_id`) REFERENCES `news` (`news_id`);

--
-- Các ràng buộc cho bảng `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FKgur68ld5yqiejudnqppqgaro5` FOREIGN KEY (`news_kind_id`) REFERENCES `news_kind` (`news_kind_id`);

--
-- Các ràng buộc cho bảng `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FKcbf93j56y7t2tyhunb4neewva` FOREIGN KEY (`gender_id`) REFERENCES `gender` (`gender_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
